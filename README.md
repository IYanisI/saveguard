# SaveGuard

## 1.Projet
##### (Ce Projet est encors inachevé et ne marchera qu'a moitié.)
SaveGuard est un script linux qui a été crée pour le projet 
infrastructure de l'école Ynov. Il a pour vocation de crée un cloud entre deux appareil avec un réseau ssh pour la communication et l'envois de donnée.

## 2.prérequis

Pour installer SaveGuard Vous devez avoir deux OS linux, un pc pour le client qui enverras ses fichier et une machine local ou distant pour les archiver.

## 3.L'Installation
 
Pour l'installation vous devez télécharger les fichier FirsTime.sh et SaveScript.sh, executer FirsTime une première fois pour ensuite déplacer SaveScript.sh dans le dossier /srv/saveguard.

## 4.les différentes étapes

le script FirsTime est la pour Installer tout ce qu'il vous faut! Mais si vous souhaiter l'installer vous-même, il faudra que vos deux machine est une clé ssh , crée un fichier de configuration qui ressemblera à [ceci](save.md), crée un nouvel utilisateur avec la commande useradd et crée un dossier saveguard pour lui donner l'accès au route /srv/saveguard avec chown, crée le service saveguard à l'emplacement /etc/systemd/system/ et écrivez-le comme [ceci](service.md), et intaller SaveScript.sh au /srv/saveguard.