[Unit]      
Description=SaveGuard       

[Service]       
ExecStart=/srv/saveguard/SaveScript.sh      
User=SaveGuard      
WorkingDirectory=/srv/saveguard     

[Install]       
WantedBy=multi-user.target          