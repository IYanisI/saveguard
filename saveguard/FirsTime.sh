#!/bin/bash
#yanis
#11/03/2024

if [ "$(id -u)" -ne 0 ]; then
echo "Vous devez être administrateur pour cette operation"
exit
fi
 
REGEXKEYTIME="^([0-9]*)(s|m|h|d)$"                                          
REGEXKEYIP="^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"
User=$(logname)
source /etc/sysconfig/network-scripts/ifcfg-enp0s3
IPADDR=$IPC >&2

if [ ! -f /home/$User/.ssh/id_rsa ]; then
ssh-keygen -b 2048 -t rsa -f /home/$User/.ssh/id_rsa

echo "OK"
fi

useradd -m SaveGuard
mkdir /srv/saveguard
chown SaveGuard:SaveGuard /srv/saveguard
touch /srv/saveguard/save.conf
while :
do
read -p "Combient de temps d'intervale voulez vous sauvegarder: " TIMES
if [[ $TIMES =~ $REGEXKEYTIME ]]
then
sed -i '/TIME/d' /srv/saveguard/save.conf
echo "TIME=$TIMES" >> /srv/saveguard/save.conf
break
else
echo "Ceci n'est pas un temps approprier, recommence."
fi
done
read -p "Comment s'appelle votre serveur: " NAMES
sed -i '/NAME/d' /srv/saveguard/save.conf
echo "NAME=$NAMES" >> /srv/saveguard/save.conf
while :
do
read -p "Entrez l'adresse ip de votre serveur: " IPS
if [[ $IPS =~ $REGEXKEYIP ]]
then
sed -i '/IP/d' /srv/saveguard/save.conf
echo "IP=$IPS" >> /srv/saveguard/save.conf
break
else
echo "Ceci n'est pas une IP, recommence."
fi
done
ssh-copy-id -i /home/$User/.ssh/id_rsa.pub $NAMES@$IPS
echo "Voulez-vous une clé ssh pour votre serveur ? y/n"
while :
do
read -p "" YN
if [ $YN = "y" ] || [ $YN = "Y" ]
then
ssh $NAMES@$IPS ssh-keygen -b 2048 -t rsa -f /home/yanis/.ssh/id_rsa
break
elif [ $YN = "n" ] || [ $YN = "N" ]
then
break
else
echo "Erreur, vous devez écrire y ou n ."
fi
done
ssh $NAMES@$IPS mkdir /home/SaveGuard
touch /etc/systemd/system/SaveGuard.service
cat > /etc/systemd/system/SaveGuard.service <<EOF
[Unit]
Description=SaveGuard

[Service]
ExecStart=/srv/saveguard/SaveScript.sh
User=SaveGuard
WorkingDirectory=/srv/saveguard

[Install]
WantedBy=multi-user.target
EOF
